import {composeWithDevTools} from 'redux-devtools-extension';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import reducers from 'reducers';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['global', 'panier', 'favorites'],
};

const persistedReducer = persistReducer(persistConfig, reducers);

export default function configureStore() {
  let enhancer;

  if (process.env.NODE_ENV === 'development') {
    enhancer = composeWithDevTools(applyMiddleware(thunk));
  } else {
    enhancer = compose(applyMiddleware(thunk));
  }

  const store = createStore(persistedReducer, enhancer);

  if (module.hot) {
    // Enable hot module replacement for reducers
    module.hot.accept(() => {
      const nextRootReducer = require('../reducers/index').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  const persist = persistStore(store);

  return {store, persist};
}
