/**
 * Copyright (c) Flexi Apps.
 *
 * A basic component to config the different app screens
 * in a router component
 *
 * Example usage:
 *
 * ```
 * import Router  from 'router';
 * ...
 *
 * <Router />
 * ```
 */

import React from 'react';
import {Router, Scene} from 'react-native-router-flux';
import {connect} from 'react-redux';
// import StackViewStyleInterpolator from 'react-navigation-stack/dist/views/StackView/StackViewStyleInterpolator';

/*
 * Routing config (from modules folder)
 */
import configureRoutes from './routingConfig';

const RouterRedux = connect()(Router);

// injected routes
const routes = configureRoutes();

const transitionConfig = () => ({
  // screenInterpolator: StackViewStyleInterpolator.forFade,
});

const AppRouter = () => {
  return (
    <RouterRedux backAndroidHandler={() => {}}>
      <Scene key="root" hideNavBar transitionConfig={transitionConfig}>
        {routes.map(({key, component, type, ...others}) => (
          <Scene key={key} component={component} type={type} {...others} />
        ))}
      </Scene>
    </RouterRedux>
  );
};

export default AppRouter;
