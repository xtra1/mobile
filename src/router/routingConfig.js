/*
 * Routing component (from modules folder)
 */
import homeRoutes from 'modules/Home/routes';
import promotionsRoutes from 'modules/Promotions/routes';
import wipRoutes from 'components/WorkInProgress/routes';

export default function configureRoutes() {
  const routes = [...homeRoutes, ...promotionsRoutes, ...wipRoutes];

  return routes;
}
