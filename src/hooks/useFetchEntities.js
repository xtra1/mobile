import {useState, useEffect} from 'react';
import {trouverEntite, trouverMeilleuresOffres} from 'api/api';
export default function useFetchEntities({
  dataType,
  text,
  typeEntite,
  orderBy,
  page,
  offreExist,
} = {}) {
  const [fetching, setFetching] = useState(false);
  const [error, setError] = useState(false);
  const [count, setCount] = useState(0);
  const [value, setValue] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setFetching(true);
        let data;
        if (dataType === 'bestOffers') {
          data = await trouverMeilleuresOffres(text, orderBy, page);
        } else {
          data = await trouverEntite(
            text,
            1,
            typeEntite,
            orderBy,
            page,
            offreExist,
          );
        }
        const {liste, rowCount} = data;
        setCount(rowCount);
        setValue(liste);
      } catch (e) {
        console.log('e', e);
        setError(true);
        // maybe display an error message
      } finally {
        setFetching(false);
      }
    };

    fetchData();
  }, [dataType, text, orderBy, page, typeEntite, offreExist]);

  return {fetching, count, error, data: value};
}
