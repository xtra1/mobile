import {PixelRatio} from 'react-native';
import colors from 'colors';

export const fontFamily = {
  defaultFont: 'Open Sans',
  defaultBold: 'OpenSans-Bold',
  primaryFont: 'Montserrat-Regular',
  secondaryFont: 'Montserrat-Light',
  boldFont: 'Montserrat-Bold',
};

export default {
  /*
   * Display in Tuto
   */
  tutoTitleText: {
    fontFamily: fontFamily.primaryFont,
    fontSize: 30 / PixelRatio.getFontScale(),
  },
  tutoStepText: {
    fontFamily: fontFamily.primaryFont,
    fontSize: 50 / PixelRatio.getFontScale(),
  },
  /*
   * Display in View header
   */
  headerText: {
    fontFamily: fontFamily.primaryFont,
    color: colors.onPrimary, // We keep the color because Header will always have primary bg
    fontSize: 20 / PixelRatio.getFontScale(),
  },
  headerTextBold: {
    fontFamily: fontFamily.boldFont,
    color: colors.onPrimary,
    fontSize: 20 / PixelRatio.getFontScale(),
  },
  /*
   * Display as surface title
   */
  titleText: {
    fontFamily: fontFamily.primaryFont,
    fontSize: 18 / PixelRatio.getFontScale(),
  },
  titleTextBold: {
    fontFamily: fontFamily.boldFont,
    fontSize: 18 / PixelRatio.getFontScale(),
  },
  titleTextLight: {
    fontFamily: fontFamily.secondaryFont,
    fontSize: 18 / PixelRatio.getFontScale(),
  },
  /*
   * Normal text in the app
   */
  bodyText: {
    fontFamily: fontFamily.primaryFont,
    fontSize: 15 / PixelRatio.getFontScale(),
  },
  bodyTextBold: {
    fontFamily: fontFamily.boldFont,
    fontSize: 15 / PixelRatio.getFontScale(),
  },
  bodyTextLight: {
    fontFamily: fontFamily.secondaryFont,
    fontSize: 15 / PixelRatio.getFontScale(),
  },
  bodyOpenSans: {
    fontFamily: fontFamily.defaultFont,
    fontSize: 15 / PixelRatio.getFontScale(),
  },
  /*
   * All button text
   */
  buttonText: {
    fontFamily: fontFamily.boldFont,
    fontSize: 15 / PixelRatio.getFontScale(),
  },
  buttonTextLight: {
    fontFamily: fontFamily.secondaryFont,
    fontSize: 15 / PixelRatio.getFontScale(),
  },
  /*
   * Small text display in surface
   */
  h3: {
    fontFamily: fontFamily.primaryFont,
    fontSize: 14 / PixelRatio.getFontScale(),
  },
  h3Bold: {
    fontFamily: fontFamily.boldFont,
    fontSize: 14 / PixelRatio.getFontScale(),
  },
  h3Light: {
    fontFamily: fontFamily.secondaryFont,
    fontSize: 14 / PixelRatio.getFontScale(),
  },
  h3OpenSans: {
    fontFamily: fontFamily.defaultFont,
    fontSize: 14 / PixelRatio.getFontScale(),
  },
  h3OpenSansBold: {
    fontFamily: fontFamily.defaultBold,
    fontSize: 14 / PixelRatio.getFontScale(),
  },
  /*
   * Small text display in surface
   */
  h4: {
    fontFamily: fontFamily.primaryFont,
    fontSize: 12 / PixelRatio.getFontScale(),
  },
  h4Bold: {
    fontFamily: fontFamily.boldFont,
    fontSize: 12 / PixelRatio.getFontScale(),
  },
  h4Light: {
    fontFamily: fontFamily.secondaryFont,
    fontSize: 12 / PixelRatio.getFontScale(),
  },
  h4OpenSans: {
    fontFamily: fontFamily.defaultFont,
    fontSize: 12 / PixelRatio.getFontScale(),
  },
  /*
   * Tiny text display in surface
   */
  h5: {
    fontFamily: fontFamily.primaryFont,
    fontSize: 10 / PixelRatio.getFontScale(),
  },
  h5Bold: {
    fontFamily: fontFamily.boldFont,
    fontSize: 10 / PixelRatio.getFontScale(),
  },
  h5Light: {
    fontFamily: fontFamily.secondaryFont,
    fontSize: 10 / PixelRatio.getFontScale(),
  },
  h5OpenSans: {
    fontFamily: fontFamily.defaultFont,
    fontSize: 10 / PixelRatio.getFontScale(),
  },
  /*
   * Tiniest text display in surface
   */
  h6: {
    fontFamily: fontFamily.primaryFont,
    fontSize: 8 / PixelRatio.getFontScale(),
  },
  h6Bold: {
    fontFamily: fontFamily.boldFont,
    fontSize: 8 / PixelRatio.getFontScale(),
  },
  h6Light: {
    fontFamily: fontFamily.secondaryFont,
    fontSize: 8 / PixelRatio.getFontScale(),
  },
  /*
   * Error text
   */
  errorText: {
    color: 'red',
    fontFamily: fontFamily.defaultFont,
    fontSize: 12 / PixelRatio.getFontScale(),
    textAlign: 'center',
    marginTop: 10,
    marginHorizontal: 5,
  },
};
