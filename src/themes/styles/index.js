import {Dimensions, Platform, PixelRatio} from 'react-native';

const {height, width, scale} = Dimensions.get('window');

export const isIphoneX =
  Platform.OS === 'ios' &&
  !Platform.isPad &&
  !Platform.isTVOS &&
  (height === 812 || width === 812);

export default {
  DeviceHeight: height,
  DeviceWidth: width,
  DeviceScale: scale,
  BorderRadius: {
    tiny: 12 / PixelRatio.getFontScale(),
    small: 14 / PixelRatio.getFontScale(),
    medium: 16 / PixelRatio.getFontScale(),
    big: 18 / PixelRatio.getFontScale(),
    large: 20 / PixelRatio.getFontScale(),
  },
  Shadow: {
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  activeOpacity: 0.6, //for TouchableOpacity when we pressed
};
