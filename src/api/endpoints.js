export function meUrl() {
  return '/me';
}

export function logoutUrl() {
  return '/auth/signout';
}

export function uploadUrl() {
  return '/files';
}
