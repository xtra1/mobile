const url = 'https://nkrent.com';

export function trouverEmploye(text, orderBy, page) {
  return fetch(url + '/listeemploye', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      // <-- Specifying the Content-Type
    }),
    body:
      'type=select&motCle=' + text + '&orderBy=' + orderBy + '&limit=' + page,
  })
    .then((response) => response.json())
    .catch((error) => {
      console.error(error);
    });
}

export function trouverEntite(
  text,
  operationel,
  typeEntite,
  orderBy,
  page,
  offreExist,
) {
  console.log(
    'text, operationel, typeEntite, orderBy, page',
    text,
    operationel,
    typeEntite,
    orderBy,
    page,
  );
  return fetch(url + '/listeentite', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      // <-- Specifying the Content-Type
    }),
    body: `type=select&motCle=${text}&operationel=${operationel}&typeEntiteNum=${typeEntite}&orderBy=${orderBy}&limit=${page}${
      offreExist ? '&offreExist=1' : ''
    }`,
  })
    .then((response) => response.json())
    .catch((error) => {
      console.error(error);
    });
}

export function trouverTypeEntite() {
  return fetch(url + '/listeentite', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      // <-- Specifying the Content-Type
    }),
    body: 'type=selectType',
  })
    .then((response) => response.json())
    .catch((error) => {
      console.error(error);
    });
}

export function trouverMeilleuresOffres(text, orderBy, page) {
  return fetch(url + '/listeentite', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      // <-- Specifying the Content-Type
    }),
    body:
      'type=select&motCle=' +
      text +
      '&offreExist=1&orderBy=' +
      orderBy +
      '&limit=' +
      page,
  })
    .then((response) => {
      return response.json();
    })
    .catch((error) => {
      console.error(error);
    });
}
