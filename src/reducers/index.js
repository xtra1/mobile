import {combineReducers} from 'redux';

/*
 * Import reducers
 */
import app from './appReducer';
import home from 'modules/Home/reducer';
import promotions from 'modules/Promotions/reducer';

/**
 * Merges the main reducer with dynamically injected reducers
 */
export default combineReducers({
  app,
  home,
  promotions,
});
