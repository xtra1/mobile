'use strict';

// declare actions types
import {REHYDRATE} from 'redux-persist/lib/constants';
import {
  SET_CURRENT_USER,
  SET_ONE_SIGNAL_ID,
  SET_SELECTED_LANGUAGE,
  SET_IS_GUEST,
} from 'actions-types';

/**
 * ## InitialState
 *
 * The fields we're concerned with
 */
const initialState = {
  currentUser: null,
  oneSignalPlayerId: null,
  isGuest: false,
};

/**
 * ## Reducer function
 * @param {Object} state - initialState
 * @param {Object} action - type and payload
 */
export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case REHYDRATE: {
      if (action.payload && action.payload.app) {
        return {...state, ...action.payload.app};
      }
      return {...state};
    }
    case SET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload,
        isGuest: false,
      };

    case SET_ONE_SIGNAL_ID:
      return {...state, oneSignalPlayerId: action.payload};

    case SET_SELECTED_LANGUAGE:
      return {...state, language: action.payload};

    case SET_IS_GUEST:
      return {...state, isGuest: action.payload};

    default:
      return state;
  }
}
