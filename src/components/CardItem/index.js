import React from 'react';
import {View, Text, ImageBackground, TouchableOpacity} from 'react-native';
import {any, string} from 'prop-types';

import styles from './styles';

const CardItem = (props) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={props.onPress}
      style={styles.container}>
      <ImageBackground source={props.image} style={styles.img}>
        <View style={styles.content}>
          <View style={styles.top} />
          <View style={styles.viewContent}>
            <Text style={styles.partner}>{props.partner}</Text>
          </View>
          <View style={styles.bottom}>
            <Text style={styles.info}>{props.tel}</Text>
            <Text
              numberOfLines={1}
              style={{...styles.info, ...styles.alignRight}}>
              {props.address}
            </Text>
          </View>
        </View>
      </ImageBackground>
      {props.discount !== 0 && (
        <View style={styles.discountView}>
          <Text style={styles.discountText}>{`-${props.discount} %`}</Text>
        </View>
      )}
    </TouchableOpacity>
  );
};

CardItem.propTypes = {
  image: any,
  partner: string,
  tel: string,
  address: string,
};

export default CardItem;
