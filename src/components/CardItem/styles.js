import common from 'styles';
import typography from 'typography';

const {DeviceWidth} = common;

export default {
  container: {
    width: DeviceWidth - 20,
    height: (DeviceWidth - 20) / 2,
    alignSelf: 'center',
    marginVertical: 5,
    borderRadius: 5,
    overflow: 'hidden',
  },
  img: {
    flex: 1,
    height: null,
    width: null,
    borderRadius: 5,
  },
  content: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  top: {
    height: 40,
    width: DeviceWidth - 20,
  },
  viewContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 10,
  },
  bottom: {
    height: 40,
    width: DeviceWidth - 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  partner: {
    ...typography.headerText,
    color: 'white',
  },
  info: {
    ...typography.h4,
    color: 'white',
    flex: 1,
  },
  alignRight: {
    textAlign: 'right',
  },
  discountView: {
    position: 'absolute',
    top: 0,
    right: 10,
    minWidth: 60,
    height: 30,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
    backgroundColor: '#ffc245',
  },
  discountText: {
    color: 'white',
    ...typography.h4,
  },
};
