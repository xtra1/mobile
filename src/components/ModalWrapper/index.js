import React from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';

import TouchExit from 'components/TouchExit';

import styles from './styles';

const ModalWrapper = ({
  children,
  onCancel,
  closable,
  topPadding,
  transparent,
}) => {
  return (
    <View
      style={{
        ...styles.container,
        ...(transparent && {backgroundColor: 'transparent'}),
      }}>
      {topPadding && (
        <TouchExit
          disabled={!closable}
          style={styles.paddings}
          onPress={() => onCancel()}
        />
      )}
      {children}
      <TouchExit
        disabled={!closable}
        style={styles.paddings}
        onPress={() => onCancel()}
      />
    </View>
  );
};

ModalWrapper.defaultProps = {
  closable: true,
  topPadding: true,
};

ModalWrapper.propTypes = {
  onCancel: PropTypes.func,
  children: PropTypes.any,
  closable: PropTypes.bool,
  topPadding: PropTypes.bool,
};

export default ModalWrapper;
