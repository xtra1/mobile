import React from 'react';
import {View, TouchableHighlight} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {Icon} from 'native-base';

// some custom hooks
import {useKeyboardAware} from 'hooks';

import styles from './styles';

const tabs = [
  {
    key: 'footer1',
    title: 'footer_home',
    icon: 'home',
    redirect: () => {
      Actions.Home();
    },
  },
  {
    key: 'footer2',
    title: 'footer_favoris',
    icon: 'heart',
    redirect: () => {
      Actions.WIP();
    },
  },
  {
    key: 'footer3',
    title: 'footer_notifs',
    icon: 'ios-notifications',
    redirect: () => {
      Actions.WIP();
    },
  },
  {
    key: 'footer4',
    title: 'footer_params',
    icon: 'ios-settings',
    redirect: () => {
      Actions.WIP();
    },
  },
];

const Footer = (props) => {
  const display = useKeyboardAware();
  return (
    display && (
      <View style={styles.container}>
        {tabs.map((tab, i) => (
          <TouchableHighlight
            key={tab.key}
            style={styles.tab}
            onPress={() => tab.redirect()}
            underlayColor="#DDDDD"
            disabled={i === props.activeTab}>
            <View style={styles.tabContent}>
              <Icon
                type="Ionicons"
                name={tab.icon}
                style={{
                  ...styles.icon,
                  ...(i === props.activeTab && styles.iconActive),
                }}
              />
            </View>
          </TouchableHighlight>
        ))}
      </View>
    )
  );
};

export default Footer;
