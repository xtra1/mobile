import colors from 'colors';
import common from 'styles';

const {DeviceHeight, DeviceWidth} = common;

export default {
  container: {
    height: DeviceHeight / 12,
    width: DeviceWidth,
    flexDirection: 'row',
  },
  tab: {
    flex: 1,
  },
  tabContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    color: colors.gray,
    fontSize: 20,
  },
  iconActive: {
    color: colors.primary,
  },
};
