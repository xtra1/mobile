import common from 'styles';
import typography from 'typography';
import colors from 'colors';

const {DeviceHeight, DeviceWidth} = common;

export default {
  container: {
    width: DeviceWidth * 0.9,
    height: DeviceHeight / 2,
    backgroundColor: 'white',
    borderRadius: 8,
    alignSelf: 'center',
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primary,
    overflow: 'hidden',
  },
  headerText: {
    color: 'white',
    ...typography.headerText,
  },
  content: {
    flex: 3,
    paddingHorizontal: '10%',
  },
  message: {
    ...typography.bodyText,
    textAlign: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btText: {
    color: 'white',
  },
  contentBottom: {
    flex: 1,
  },
  contentText: {
    width: '100%',
    minHeight: DeviceHeight / 6,
    justifyContent: 'center',
  },
  infoContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  infoText: {
    marginLeft: 12,
    ...typography.bodyText,
  },
};
