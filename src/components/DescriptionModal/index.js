import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';

import {openUrl, openGoogleMap} from 'lib/helpers';

// some components
import ModalWrapper from 'components/ModalWrapper';
import Button from 'components/Button';

import styles from './styles';

const Description = (props) => {
  return (
    props.display && (
      <ModalWrapper onCancel={props.onCancel}>
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.headerText}> {props.item?.nom} </Text>
          </View>
          <View style={styles.content}>
            <View style={styles.contentText}>
              <Text style={styles.message}>{props.item.offre.description}</Text>
            </View>
            <View style={styles.contentBottom}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() =>
                  openGoogleMap(
                    `${props.item.latitude},${props.item.longitude}`,
                  )
                }
                style={styles.infoContainer}>
                <AutoHeightImage
                  source={require('assets/imgs/pin.png')}
                  width={20}
                />
                <Text style={styles.infoText}>{props.item.adresse}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => openUrl(`tel:${props.item.telephone}`)}
                style={styles.infoContainer}>
                <AutoHeightImage
                  source={require('assets/imgs/phone.png')}
                  width={20}
                />
                <Text style={styles.infoText}>{props.item.telephone}</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.footer}>
            <Button primary size="small" type="filled" onPress={props.onCancel}>
              <Text style={styles.btText}> Ok </Text>
            </Button>
          </View>
        </View>
      </ModalWrapper>
    )
  );
};

export default Description;
