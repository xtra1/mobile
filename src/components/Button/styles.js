'use strict';
import common from 'styles';
import colors from 'colors';

const {DeviceWidth} = common;

export default {
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    height: 40,
    borderWidth: 1,
    paddingHorizontal: 8,
  },
  primary: {
    borderColor: colors.primary,
    backgroundColor: colors.primary,
  },
  secondary: {
    borderColor: colors.secondary,
    backgroundColor: colors.secondary,
  },
  outlined: {
    backgroundColor: 'white',
  },
  large: {
    width: DeviceWidth * (3 / 4),
  },
  small: {
    width: DeviceWidth / 3,
  },
  disabled: {
    position: 'absolute',
    top: -1,
    left: -1,
    bottom: -1,
    right: -1,
    backgroundColor: 'rgba(255,255,255,0.5)',
    borderRadius: 20,
  },
};
