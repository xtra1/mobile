/**
 * Copyright (c) Flexi Apps.
 *
 * A component to render buttons.
 *
 * Example usage:
 *
 * ```
 * import Button from 'components/Button';
 * ...
 *
 * <Button
 *  primary
 *  onPress={()=>{}}
 *  style={{margin: 10}}
 *  type="filled"
 *  size="large"
 * >
 *  <Image />
 * </Button>
 * ```
 */

'use strict';

import React from 'react';
import {View} from 'react-native';
import {func, any, oneOf, string, bool, object} from 'prop-types';

import BaseButton from './Base';

import styles from './styles';

//TODO : make button bgColor customizable
const Button = (props) => {
  return (
    <BaseButton
      containerStyle={{
        ...styles.container,
        ...(props.size && styles[props.size]),
        ...(props.primary && styles.primary),
        ...(props.secondary && styles.secondary),
        ...(props.color && {
          borderColor: props.color,
          backgroundColor: props.color,
        }),
        ...(props.type === 'outlined' && styles.outlined),
        ...(props.style && props.style),
      }}
      onPress={props.onPress}
      disabled={props.disabled}>
      {props.children}
      {props.disabled && <View style={styles.disabled} />}
    </BaseButton>
  );
};

Button.propTypes = {
  size: oneOf(['large', 'small']).isRequired,
  type: oneOf(['filled', 'outlined']).isRequired,
  primary: bool,
  secondary: bool,
  color: string,
  style: object,
  children: any.isRequired,
  disabled: bool,
  onPress: func,
};

export default Button;
