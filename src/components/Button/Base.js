/**
 * Copyright (c) Flexi Apps.
 *
 * A component to implement button Base component.
 *
 * Example usage:
 *
 * ```
 * import BaseButton from 'components/BaseButton';
 * ...
 *
 * <BaseButton
 *  onPress={()=>{}}
 *  children={[]}
 * />
 * ```
 */

'use strict';

import React from 'react';
import {TouchableOpacity} from 'react-native';
import {object, any, bool} from 'prop-types';

import styles from './styles';

const BaseButton = (props) => {
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={props.onPress}
      disabled={props.disabled}
      style={{...styles.container, ...props.containerStyle}}>
      {props.children}
    </TouchableOpacity>
  );
};

BaseButton.propTypes = {
  containerStyle: object,
  children: any,
  disabled: bool,
};

export default BaseButton;
