/*
 * Routing component (from modules folder)
 */
import WIP from 'components/WorkInProgress';

const routes = [
  {
    key: 'WIP',
    component: WIP,
  },
];

export default routes;
