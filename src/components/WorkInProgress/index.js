import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';
import {Icon} from 'native-base';
import {Actions} from 'react-native-router-flux';

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  back: {
    position: 'absolute',
    top: 8,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 40,
  },
  icon: {
    fontSize: 30,
    color: 'black',
  },
  text: {
    marginTop: 20,
  },
};

const WIP = () => (
  <View style={styles.container}>
    <AutoHeightImage source={require('assets/imgs/logo.png')} width={80} />
    <Text style={styles.text}> Module en cours de développement </Text>
    <TouchableOpacity style={styles.back} onPress={() => Actions.pop()}>
      <Icon name="arrow-back-outline" style={styles.icon} />
    </TouchableOpacity>
  </View>
);

export default WIP;
