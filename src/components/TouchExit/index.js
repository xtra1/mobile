import React from 'react';
import {TouchableOpacity} from 'react-native';
import {object, func, bool} from 'prop-types';

const TouchExit = (props) => (
  <TouchableOpacity
    style={props.style}
    activeOpacity={1}
    onPress={props.onPress}
    disabled={props.disabled}
  />
);

TouchExit.propsTypes = {
  style: object,
  onPress: func,
  disabled: bool,
};

export default TouchExit;
