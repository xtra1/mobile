import colors from 'colors';
import common from 'styles';

const {DeviceHeight, DeviceWidth} = common;

export default {
  container: {
    height: DeviceHeight / 12,
    width: DeviceWidth,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.primary,
    flexDirection: 'row',
  },
  side: {
    height: '100%',
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.primary,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  search: {
    color: colors.onPrimary,
  },
  backIcon: {
    fontSize: 30,
    color: colors.onPrimary,
  },
  searchInput: {
    height: 40,
    width: DeviceWidth * (3 / 4),
  },
  searchText: {
    color: colors.onPrimary,
  },
};
