import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Icon, Item, Input} from 'native-base';
import AutoHeightImage from 'react-native-auto-height-image';

// some custom hooks

import styles from './styles';
import {Actions} from 'react-native-router-flux';

const Header = (props) => {
  const [searching, setSearching] = useState(false);
  const [query, setQuery] = useState('');

  const onSearch = (t) => {
    setQuery(t);
    props.onSearch(t);
  };

  return (
    <View style={styles.container}>
      {props.back && (
        <TouchableOpacity
          style={styles.side}
          activeOpacity={0.8}
          onPress={() => Actions.pop()}>
          <Icon name="arrow-back" style={styles.search} />
        </TouchableOpacity>
      )}
      <AutoHeightImage source={require('assets/imgs/logo.png')} width={40} />
      <TouchableOpacity
        style={styles.side}
        activeOpacity={0.8}
        onPress={() => setSearching(true)}>
        <Icon name="search" style={styles.search} />
      </TouchableOpacity>
      {searching && (
        <View style={styles.searchContainer}>
          <TouchableOpacity
            style={styles.side}
            activeOpacity={0.8}
            onPress={() => {
              setSearching(false);
              onSearch('');
            }}>
            <Icon name="arrow-back-outline" style={styles.backIcon} />
          </TouchableOpacity>
          <Item rounded style={styles.searchInput}>
            <Icon name="search" style={styles.search} />
            <Input
              style={styles.searchText}
              value={query}
              onChangeText={onSearch}
              placeholder="Rechercher"
              placeholderTextColor={styles.searchText.color}
            />
          </Item>
          <View style={styles.side} />
        </View>
      )}
    </View>
  );
};

Header.defaultProps = {
  back: true,
};

export default Header;
