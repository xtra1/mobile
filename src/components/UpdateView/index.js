/*
 *
 * CODEPUSH UPDATE VIEW
 */
import React from 'react';
import {View, Text} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';
import {Bar} from 'react-native-progress';
import PropTypes from 'prop-types';
import colors from 'colors';
import common from 'styles';
import typography from 'typography';
import strings from 'config/strings';

const {DeviceWidth, DeviceHeight} = common;

const styles = {
  container: {
    flex: 1,
    backgroundColor: colors.primaryVariant,
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    marginVertical: DeviceHeight / 10,
  },
  updateText: {
    ...typography.bodyText,
    color: colors.onPrimary,
    marginBottom: 20,
  },
};

const UpdateView = (props) => {
  return (
    <View style={styles.container}>
      <AutoHeightImage
        style={styles.img}
        width={200}
        source={require('assets/imgs/logo.png')}
      />
      <Text style={styles.updateText}>{strings.updating}</Text>
      <Bar
        progress={props.percent / 100}
        width={DeviceWidth / 2}
        color={'white'}
      />
    </View>
  );
};

UpdateView.propTypes = {
  percent: PropTypes.number,
};

export default UpdateView;
