import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';

import styles from './styles';

const Category = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.container}>
      {props.icon ? <AutoHeightImage source={props.icon} width={30} /> : null}
      <Text style={styles.name}>{props.name}</Text>
    </TouchableOpacity>
  );
};

export default Category;
