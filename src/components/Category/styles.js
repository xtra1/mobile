import common from 'styles';
import typography from 'typography';
import colors from 'colors';

const {DeviceHeight, DeviceWidth, Shadow} = common;

export default {
  container: {
    width: DeviceWidth / 4,
    height: DeviceWidth / 4,
    borderRadius: DeviceWidth / 8,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    ...Shadow,
  },
  name: {
    ...typography.h4,
    color: colors.primary,
    textAlign: 'center',
  },
};
