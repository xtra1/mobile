import React from 'react';
import {View} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';

// some custom hooks

import styles from './styles';

const Header = (props) => {
  return (
    <View style={styles.container}>
      <AutoHeightImage source={require('assets/imgs/logo.png')} width={40} />
    </View>
  );
};

export default Header;
