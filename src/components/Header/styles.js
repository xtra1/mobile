import colors from 'colors';
import common from 'styles';

const {DeviceHeight, DeviceWidth} = common;

export default {
  container: {
    height: DeviceHeight / 12,
    width: DeviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primary,
  },
};
