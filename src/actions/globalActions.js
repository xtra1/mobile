// Api
import {Api} from 'api';

// some endpoints
import {meUrl, uploadUrl} from 'api/endpoints';

import {displaySessionToast} from 'lib/interactions';
import reportError from 'lib/errorHandler';

// import actions types
import {SET_ONE_SIGNAL_ID, SET_CURRENT_USER, SET_IS_GUEST} from 'actions-types';

export const setCurrentUser = (payload) => ({
  type: SET_CURRENT_USER,
  payload,
});

export const setIsGuest = (payload) => ({
  type: SET_IS_GUEST,
  payload,
});

export const setOneSignalId = (payload) => ({
  type: SET_ONE_SIGNAL_ID,
  payload,
});

export const getMe = () => {
  return (dispatch) => {
    Api()
      .get(meUrl())
      .then((data) => {
        if (data) {
          console.log('data', data);
          dispatch(setCurrentUser(data));
        } else {
          displaySessionToast();
        }
      })
      .catch((err) => {
        reportError('get me error', err);
      });
  };
};

export const uploadFiles = (payload) => {
  const promise = new Promise(async (resolve, reject) => {
    try {
      const data = await Api().post(uploadUrl(), payload, true);
      resolve(data);
    } catch (err) {
      reportError('upload files error ', err);
      reject(err);
    }
  });

  return promise;
};
