import LocalizedStrings from 'react-native-localization';

import fr from 'locales/fr';

let strings = new LocalizedStrings({
  fr,
});

export default strings;
