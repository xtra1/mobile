export default {
  BASE_URL: 'https://nkrent.com',
  API_BASE_URL: 'https://nkrent.com',
  API_VERSION: '/v1',
  REQUEST_TIMER: 30000,
  POSTMAN_COLLECTION: '',
};
