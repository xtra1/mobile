/*
 * Routing component (from modules folder)
 */
import Home from 'modules/Home';

const routes = [
  {
    key: 'Home',
    component: Home,
    initial: true,
  },
];

export default routes;
