import React, {useEffect, useState} from 'react';
import {View, Text, FlatList} from 'react-native';
import {Actions} from 'react-native-router-flux';

// some components
import Footer from 'components/Footer';
import Header from 'components/Header';
import Button from 'components/Button';
import Category from 'components/Category';

import strings from 'config/strings';
import {trouverTypeEntite} from 'api/api';

import styles from './styles';

const HomePage = (props) => {
  const [categories, setCategories] = useState([]);
  const [fetching, setFetching] = useState(false);
  const [count, setCount] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setFetching(true);
        const {liste, rowCount} = await trouverTypeEntite();
        setCategories(liste);
        setCount(rowCount);
      } catch (e) {
      } finally {
        setFetching(false);
      }
    };

    fetchData();
  }, []);

  const openBestOffers = () => {
    props.actions.setDataTypes({id: -1, designation: 'bestOffers'});
    Actions.Promotions();
  };

  const _keyExtractor = (item) => item.id.toString();

  const onRefresh = () => {
    // refresh the list
  };

  const onEndReached = () => {
    // fetch new data with paging
  };

  const renderItem = ({item}) => {
    return (
      <Category
        icon={item.image}
        name={item.designation}
        onPress={() => {
          props.actions.setDataTypes({
            id: item.id,
            designation: item.designation,
          });
          Actions.Promotions();
        }}
      />
    );
  };

  const renderFooter = () => null;

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.content}>
        <View style={styles.categories}>
          <FlatList
            data={categories}
            renderItem={renderItem}
            keyExtractor={_keyExtractor}
            showsVerticalScrollIndicator={false}
            ListFooterComponent={renderFooter}
            onRefresh={onRefresh}
            refreshing={fetching}
            onEndReached={onEndReached}
            onEndReachedThreshold={10}
            keyboardShouldPersistTaps="always"
            numColumns={2}
          />
        </View>
        <View style={styles.footer}>
          <Button primary size="large" type="filled" onPress={openBestOffers}>
            <Text style={styles.btText}> {strings.home_bestoffers} </Text>
          </Button>
        </View>
      </View>
      <Footer activeTab={0} />
    </View>
  );
};

export default HomePage;
