import colors from 'colors';

export default {
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  categories: {
    flex: 4,
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btText: {
    color: colors.onPrimary,
  },
};
