/**
 * Copyright (c) Flexi Apps.
 *
 * App Home page view container.
 *
 */

'use strict';

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// redux action
import * as actions from './actions';
import {getMe} from 'actions';
import {setDataTypes} from 'modules/Promotions/actions';

import HomeComponent from './components';

const Home = (props) => {
  return <HomeComponent {...props} />;
};

// map state to props
const mapStateToProps = ({app, home}) => ({
  currentUser: app.currentUser,
  isGuest: app.isGuest,
  home,
});

// map dispatch to props
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(
    {
      ...actions,
      getMe,
      setDataTypes,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
