'use strict';

// declare actions types
import {REHYDRATE} from 'redux-persist/lib/constants';
import {
  GET_CATS_RESQUEST,
  GET_CATS_SUCCESS,
  GET_CATS_FAILURE,
} from './actionsTypes';

/**
 * ## InitialState
 *
 * The fields we're concerned with
 */
const initialState = {
  categories: [],
  fetching: false,
  count: 0,
};

/**
 * ## Reducer function
 * @param {Object} state - initialState
 * @param {Object} action - type and payload
 */
export default function home(state = initialState, action) {
  switch (action.type) {
    case REHYDRATE: {
      if (action.payload && action.payload.home) {
        return {...state, ...action.payload.home};
      }
      return {...state};
    }

    case GET_CATS_RESQUEST:
      return {...state, fetching: true};

    case GET_CATS_SUCCESS:
      return {
        ...state,
        fetching: false,
        count: action.payload.rowCount,
        categories: action.payload.liste,
      };

    case GET_CATS_FAILURE:
      return {...state, fetching: false};

    default:
      return state;
  }
}
