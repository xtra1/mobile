/**
 * Copyright (c) Flexi Apps.
 *
 * App Home page view container.
 *
 */

'use strict';

import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

// redux action
import * as actions from './actions';

import PromosComponent from './components';

const Promotions = (props) => {
  return <PromosComponent {...props} />;
};

// map state to props
const mapStateToProps = ({app, promotions}) => ({
  currentUser: app.currentUser,
  isGuest: app.isGuest,
  promotions,
});

// map dispatch to props
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(
    {
      ...actions,
    },
    dispatch,
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(Promotions);
