import colors from 'colors';
import typography from 'typography';

export default {
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  categories: {
    flex: 3,
  },
  footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btText: {
    color: colors.onPrimary,
  },
  footerView: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 60,
    backgroundColor: 'rgba(0,0,0,0.1)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  filterView: {
    height: 30,
    width: 100,
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    ...typography.h4,
    marginRight: 10,
  },
  icon: {
    fontSize: 20,
  },
  listFooter: {
    width: '100%',
    height: 80,
  },
};
