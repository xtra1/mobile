import React, {useEffect, useState} from 'react';
import {View, Text, FlatList, TouchableOpacity} from 'react-native';
import {Icon} from 'native-base';
import {Actions} from 'react-native-router-flux';

import CONFIG from 'config/api';

// some components
import Header from 'components/HeaderSearch';
import Button from 'components/Button';
import CardItem from 'components/CardItem';
import Description from 'components/DescriptionModal';

import strings from 'config/strings';
import useFetchEntities from 'hooks/useFetchEntities';

import styles from './styles';

const {BASE_URL} = CONFIG;

const Promos = (props) => {
  const {dataType} = props.promotions;
  const [page, setPage] = useState(0);
  const [query, setQuery] = useState(0);
  const [current, setCurrent] = useState(null);
  const [displayModal, setDisplayModal] = useState(false);

  const {fetching, count, error, data} = useFetchEntities({
    dataType: dataType.id === -1 ? '' : dataType.id,
    text: query,
    typeEntite: dataType.id === -1 ? '' : dataType.id,
    orderBy: '',
    page,
    ...(dataType.id === -1 && {offreExist: true}),
  });

  const _keyExtractor = (item, index) => `index-${item.id}`;

  const onRefresh = () => {
    // refresh the list
  };

  const onEndReached = () => {
    // fetch new data with paging
  };

  const renderItem = ({item}) => {
    return (
      <CardItem
        image={{uri: `${BASE_URL}/inc/entite/${item.id}${item.photo}`}}
        partner={item.nom}
        tel={item.telephone}
        address={`${item.adresse} ${item.ville}`}
        discount={item.offre?.pourcentage || 0}
        onPress={() => {
          setCurrent(item);
          setDisplayModal(true);
        }}
      />
    );
  };

  const renderFooter = () => <View style={styles.listFooter} />;

  return (
    <View style={styles.container}>
      <Header onSearch={(t) => setQuery(t)} />
      <View style={styles.content}>
        <FlatList
          data={data}
          renderItem={renderItem}
          keyExtractor={_keyExtractor}
          showsVerticalScrollIndicator={false}
          ListFooterComponent={renderFooter}
          onRefresh={onRefresh}
          refreshing={fetching}
          onEndReached={onEndReached}
          onEndReachedThreshold={10}
          keyboardShouldPersistTaps="always"
        />
      </View>
      <View style={styles.footerView}>
        <TouchableOpacity style={styles.filterView}>
          <Text style={styles.text}>{strings.filtres}</Text>
          <Icon name="options-outline" style={styles.icon} />
        </TouchableOpacity>
      </View>
      {displayModal && (
        <Description
          display={displayModal}
          item={current}
          onCancel={() => setDisplayModal(false)}
        />
      )}
    </View>
  );
};

export default Promos;
