/*
 * Routing component (from modules folder)
 */
import Promotions from 'modules/Promotions';

const routes = [
  {
    key: 'Promotions',
    component: Promotions,
  },
];

export default routes;
