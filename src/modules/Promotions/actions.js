import {SET_DATA_TYPES} from './actionsTypes';

export const setDataTypes = (payload) => ({
  type: SET_DATA_TYPES,
  payload,
});
