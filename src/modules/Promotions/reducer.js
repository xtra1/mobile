'use strict';

// declare actions types
import {REHYDRATE} from 'redux-persist/lib/constants';
import {SET_DATA_TYPES} from './actionsTypes';

/**
 * ## InitialState
 *
 * The fields we're concerned with
 */
const initialState = {
  data: [],
  dataType: '',
  fetching: false,
};

/**
 * ## Reducer function
 * @param {Object} state - initialState
 * @param {Object} action - type and payload
 */
export default function promos(state = initialState, action) {
  switch (action.type) {
    case REHYDRATE: {
      if (action.payload && action.payload.promotions) {
        return {...state, ...action.payload.promotions};
      }
      return {...state};
    }

    case SET_DATA_TYPES:
      return {...state, dataType: action.payload};

    default:
      return state;
  }
}
