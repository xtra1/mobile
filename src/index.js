'use strict';

import React from 'react';

import {Provider} from 'react-redux';
import OneSignal from 'react-native-onesignal';

import {MenuProvider} from 'react-native-popup-menu';

// import configureSentry from 'config/sentry';

import notificationHandler from 'lib/notificationHandler';

// Some actions
import {setOneSignalId} from 'actions';

import configureStore from './store';
import Router from './router';

const {store} = configureStore();

export default class Main extends React.Component {
  componentDidMount() {
    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.inFocusDisplaying(2);

    // this.sentry = configureSentry();
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
  }

  onReceived = () => {};

  onOpened = (openResult) => {
    const {additionalData, isAppInFocus} = openResult.notification.payload;
    const {dispatch} = store;
    notificationHandler({additionalData, isAppInFocus}, dispatch);
  };

  onIds = (device) => {
    store.dispatch(setOneSignalId(device.userId));
  };

  render() {
    return (
      <MenuProvider skipInstanceCheck>
        <Provider store={store}>
          <Router />
        </Provider>
      </MenuProvider>
    );
  }
}
