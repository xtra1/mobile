export default {
  /******************************* GENERAL  **************************************** */
  sessionExpired: 'Votre session a expiré',

  updating: 'Mise à jour en cours...',
  /** ********************************* LANGUAGES **************************************** */
  en: 'English',
  fr: 'Français',

  /** ********************************* HOME **************************************** */
  home_bestoffers: 'Les meilleurs offres',

  filtres: 'filtres',
};
