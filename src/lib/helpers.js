'use strict';
import {Linking} from 'react-native';

export const openUrl = (url) => {
  Linking.canOpenURL(url)
    .then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    })
    .catch((err) => console.log('An error occurred', err));
};

export const openGoogleMap = (location) => {
  const url = `https://www.google.com/maps/search/?api=1&query=${location}`;
  openUrl(url);
};
