/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {Platform, SafeAreaView, StatusBar} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import OneSignal from 'react-native-onesignal';
import codePush from 'react-native-code-push';

import UpdateView from 'components/UpdateView';

import onesignalConfig from 'config/onesignal';

import Main from './src';

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE,
};

const styles = {
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
};

class App extends React.Component {
  state = {
    codePushUpdating: false,
    percent: 0,
  };

  componentDidMount() {
    if (Platform.OS === 'android') {
      SplashScreen.hide();
    }
    OneSignal.init(onesignalConfig.APP_ID, {
      kOSSettingsKeyInFocusDisplayOption: true,
    });
  }

  codePushStatusDidChange(status) {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log('Checking for updates.');
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        this.setState({codePushUpdating: true});
        break;
      case codePush.SyncStatus.INSTALLING_UPDATE:
        break;
      case codePush.SyncStatus.UP_TO_DATE:
        console.log('Up to date.');
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED:
        console.log('Update installed.');
        break;
      default:
        break;
    }
  }

  codePushDownloadDidProgress(progress) {
    this.setState({
      percent: parseInt(
        (progress.receivedBytes * 100) / progress.totalBytes,
        10,
      ),
    });
  }

  render() {
    const {codePushUpdating, percent} = this.state;
    return (
      <>
        <StatusBar /*backgroundColor={app.darkPink}*/ />
        <SafeAreaView style={styles.container}>
          {codePushUpdating ? <UpdateView percent={percent} /> : <Main />}
        </SafeAreaView>
      </>
    );
  }
}

export default codePush(codePushOptions)(App);
